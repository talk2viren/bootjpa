package com.thetechblogs.bootjpa.entity;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;


@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
//@IdClass()
public class StudentKey implements Serializable {
    private int studentId;
    private String group;

}
