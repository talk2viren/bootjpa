package com.thetechblogs.bootjpa.controller;

import com.github.javafaker.Faker;
import com.thetechblogs.bootjpa.entity.Student;
import com.thetechblogs.bootjpa.entity.StudentKey;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class StudentController {

    @GetMapping("/studentList")
    public ResponseEntity<List<Student>> getAllStudents(){
        Faker faker=null;
        Student student=null;
        List<Student> studentList=new ArrayList<>();

         for(int i=0;i<20;i++){
            faker =new Faker();
             student= new Student();
//             student.setStudentKey(new StudentKey(i,"group_"+i));
             student.setStudentId(i);
             student.setStudentCity(faker.address().city());
             student.setName(faker.name().firstName());
             studentList.add(student);
         }

        return new ResponseEntity<List<Student>>(studentList, HttpStatus.MULTI_STATUS);
    }
}
