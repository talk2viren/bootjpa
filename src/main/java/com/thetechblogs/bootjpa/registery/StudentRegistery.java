package com.thetechblogs.bootjpa.registery;

import com.thetechblogs.bootjpa.entity.Student;
import com.thetechblogs.bootjpa.entity.StudentKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRegistery extends JpaRepository<Student, StudentKey> {
}
